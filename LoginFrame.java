package terminal;

import java.awt.CardLayout;
import java.awt.Container;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;

import terminal.loginFramePanels.AboutPanel;
import terminal.loginFramePanels.IconPanel;
import terminal.loginFramePanels.LoginPanel;
import terminal.loginFramePanels.RegisterPanel;
import utils.DBConnection;
import utils.ProjectColors;
import utils.ProjectIcon;

@SuppressWarnings("serial")
public class LoginFrame extends JFrame{
	
		private JPanel iconRegisterPanel;
	    private JPanel IRPanel;
	    private JPanel aboutRegisterP;
	    private Decorated decorated;
	    private IconPanel iconPanel; 
	    private RegisterPanel registerPanel;
	    private AboutPanel aboutPanel;
	    private LoginPanel loginPanel;
		private JPanel mainLoginPanel;
		public static boolean isActive;

    public LoginFrame() {
        getActiveStatus();
        setUndecorated(true);
        initComponents();
        createUIComponents();
        setVisible(true);
    }


    private void getActiveStatus() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection =DBConnection.getConnection(iconRegisterPanel);
            statement = connection.createStatement();
            String sql = "select status from licenses where license_code = 1265365663;";
            resultSet = statement.executeQuery(sql);
            resultSet.next();

            isActive = resultSet.getBoolean(1);


        } catch (Exception e) {

        }

    }



    private void createUIComponents() {
       
            iconRegisterPanel.setBackground(ProjectColors.BACKGROUND.color());
            aboutRegisterP.setBackground(ProjectColors.BACKGROUND.color());  
    }

    private void initComponents() {
    	IRPanel = new JPanel();
        iconPanel = new IconPanel();
        registerPanel = new RegisterPanel();
        aboutPanel = new AboutPanel();
       
        iconPanel.setIRPanel(IRPanel);

        iconPanel.setAboutPanel(aboutPanel);
        iconPanel.setRegisterPanel(registerPanel);
       
    	aboutPanel.setIRPanel(IRPanel);
    	aboutPanel.setIconPanel(iconPanel);
    	
        registerPanel.setIRPanel(IRPanel);
        registerPanel.setIconPanel(iconPanel);
        
        iconRegisterPanel = new JPanel();
        mainLoginPanel = new JPanel();
        aboutRegisterP = new JPanel();
       
        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setIconImage(ProjectIcon.PISTACHIO_LOGO_SMALL.icon().getImage());
        Container contentPane = getContentPane();

        //======== iconRegisterPanel ========
        {
            iconRegisterPanel.setBorder(null);

            //======== panel1 ========
            {
                mainLoginPanel.setBorder(null);
                mainLoginPanel.setLayout(new CardLayout());
            }

            //======== IRPanel ========
            {
                IRPanel.setBorder(null);
                IRPanel.setLayout(new CardLayout());
                IRPanel.add(aboutPanel);
                IRPanel.add(iconPanel , "card2");
                IRPanel.add(registerPanel , "card1");
                IRPanel.add(aboutPanel, "card3");
            }

            GroupLayout iconRegisterPanelLayout = new GroupLayout(iconRegisterPanel);
            iconRegisterPanel.setLayout(iconRegisterPanelLayout);
            iconRegisterPanelLayout.setHorizontalGroup(
                iconRegisterPanelLayout.createParallelGroup()
                    .addGroup(iconRegisterPanelLayout.createSequentialGroup()
                        .addGap(500, 500, 500)
                        .addComponent(mainLoginPanel, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
                    .addComponent(IRPanel, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
            );
            iconRegisterPanelLayout.setVerticalGroup(
                iconRegisterPanelLayout.createParallelGroup()
                    .addComponent(mainLoginPanel, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
                    .addComponent(IRPanel, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
            );
        }
        
        loginPanel = new LoginPanel(this);
        this.mainLoginPanel.add(this.loginPanel, "name_179415880910198");
         
        this.decorated = new Decorated();


        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPaneLayout.setHorizontalGroup(
        	contentPaneLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(contentPaneLayout.createSequentialGroup()
        			.addGroup(contentPaneLayout.createParallelGroup(Alignment.LEADING)
        				.addComponent(this.iconRegisterPanel, GroupLayout.PREFERRED_SIZE, 903, GroupLayout.PREFERRED_SIZE)
        				.addComponent(this.decorated, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
        	contentPaneLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(contentPaneLayout.createSequentialGroup()
        			.addComponent(this.decorated, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(this.iconRegisterPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        contentPane.setLayout(contentPaneLayout);
        pack();
        setLocationRelativeTo(getOwner());

    }

}
