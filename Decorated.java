

package terminal;

import utils.FixedStateButtonModel;
import utils.ProjectColors;
import utils.ProjectIcon;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import utils.ClockLabel;


public class Decorated extends JPanel implements MouseListener{

    private  Point compCoords = null;

    public Decorated() {

        initComponents();
        createUIComponents();
    }

    private void createUIComponents() {
        // TODO: add custom component creation code here
        closeButton.setBackground(ProjectColors.BACKGROUND.color());
        closeButton.setContentAreaFilled(false);
        closeButton.setModel(new FixedStateButtonModel());
        closeButton.addMouseListener(this);

        minButton.setBackground(ProjectColors.BACKGROUND.color());
        minButton.setContentAreaFilled(false);
        minButton.setModel(new FixedStateButtonModel());
        minButton.addMouseListener(this);

        JFrame parentFrame = (JFrame) SwingUtilities.getRoot(this);

        Map <Integer,String> a =new HashMap<Integer , String>();


        closeButton.addActionListener(

        		e -> {System.exit(0);
        });

        minButton.addActionListener(e -> {
            Component component = (Component) e.getSource();
            JFrame frame = (JFrame) SwingUtilities.getRoot(component);
            frame.setState(JFrame.ICONIFIED);});

        opratePanel.addMouseListener(this);
        opratePanel.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                Component component = (Component) e.getSource();
                JFrame frame = (JFrame) SwingUtilities.getRoot(component);
                Point currCoords = e.getLocationOnScreen();
                frame.setLocation(currCoords.x - compCoords.x , currCoords.y - compCoords.y);
            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });

    }

    public void setOpPanelBackGround(Color color) {
    	opratePanel.setBackground(color);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        compCoords = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        compCoords = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    	if(e.getSource() == closeButton)
    		closeButton.setIcon(ProjectIcon.CLOSE_ICON_RED.icon());
    	else if(e.getSource() == minButton)
    		minButton.setIcon(ProjectIcon.MIN_ICON_BLUE.icon());

    }

    @Override
    public void mouseExited(MouseEvent e) {

    	if(e.getSource() == closeButton)
    		closeButton.setIcon(ProjectIcon.CLOSE_ICON_GREY.icon());
    	else if(e.getSource() == minButton)
    		minButton.setIcon(ProjectIcon.MIN_ICON_GREY.icon());

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        opratePanel = new JPanel();
        closeButton = new JButton();
        this.closeButton.setFocusable(false);
        this.closeButton.setFocusPainted(false);
        minButton = new JButton();
        this.minButton.setFocusable(false);
        this.minButton.setFocusPainted(false);

        //======== this ========
        setPreferredSize(new Dimension(900, 30));
        setMinimumSize(new Dimension(244, 30));
        setLayout(new BorderLayout());

        //======== opratePanel ========
        {
            opratePanel.setMinimumSize(new Dimension(238, 30));

            //---- closeButton ----
            closeButton.setIcon(new ImageIcon(Decorated.class.getResource("/icons/icons8_Close_Window_32px_11.png")));
            closeButton.setBorder(null);

            //---- minButton ----
            minButton.setBorder(null);
            minButton.setIcon(new ImageIcon(Decorated.class.getResource("/icons/icons8_Minimize_Window_32px_1.png")));
            this.clcklblMonApr = new ClockLabel();

            GroupLayout opratePanelLayout = new GroupLayout(opratePanel);
            opratePanelLayout.setHorizontalGroup(
            	opratePanelLayout.createParallelGroup(Alignment.TRAILING)
            		.addGroup(opratePanelLayout.createSequentialGroup()
            			.addContainerGap()
            			.addComponent(this.clcklblMonApr, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
            			.addPreferredGap(ComponentPlacement.RELATED, 570, Short.MAX_VALUE)
            			.addComponent(this.minButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
            			.addPreferredGap(ComponentPlacement.RELATED)
            			.addComponent(this.closeButton))
            );
            opratePanelLayout.setVerticalGroup(
            	opratePanelLayout.createParallelGroup(Alignment.LEADING)
            		.addGroup(opratePanelLayout.createSequentialGroup()
            			.addGroup(opratePanelLayout.createParallelGroup(Alignment.LEADING)
            				.addGroup(opratePanelLayout.createParallelGroup(Alignment.LEADING, false)
            					.addComponent(this.minButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            					.addComponent(this.closeButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            				.addComponent(this.clcklblMonApr, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            opratePanel.setLayout(opratePanelLayout);
        }
        add(opratePanel, BorderLayout.CENTER);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel opratePanel;
    private JButton closeButton;
    private JButton minButton;
    private ClockLabel clcklblMonApr;

}


