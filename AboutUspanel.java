package terminal.menuFramePanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import utils.FixedStateButtonModel;
import utils.ProjectColors;
import utils.ProjectFont;
import utils.ProjectIcon;
import javax.swing.LayoutStyle.ComponentPlacement;

public class AboutUspanel extends JPanel {

	private JLabel logoLabel;
	private JLabel emailLabel;
	private JLabel phoneLabel;
	private JLabel addLabel;
	private JLabel siteAddLabel;
	private JLabel titleLabel;

	private JPanel IRPanel ,iconPanel;
	
	public AboutUspanel() {

		initGUI();
		myInitGUI();
	}
	
	
	public JPanel getIRPanel() {
		return IRPanel;
	}

	public void setIRPanel(JPanel iRPanel) {
		IRPanel = iRPanel;
	}

	public JPanel getIconPanel() {
		return iconPanel;
	}

	public void setIconPanel(JPanel iconPanel) {
		this.iconPanel = iconPanel;
	}

	private void myInitGUI() {
		
		setBackground(ProjectColors.BACKGROUND.color());
		

        
        
        emailLabel.setIcon(new ImageIcon(getClass().getResource("/icons/njbhhb.png")));
        emailLabel.setHorizontalAlignment(SwingConstants.LEFT);
        emailLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
        emailLabel.setVerticalTextPosition(SwingConstants.TOP);
        emailLabel.setText("      ");

        
        phoneLabel.setIcon(new ImageIcon(getClass().getResource("/icons/vvvv.png")));

     
        siteAddLabel.setIcon(new ImageIcon(getClass().getResource("/icons/bbbbbb.png")));

    
        addLabel.setIcon(new ImageIcon(getClass().getResource("/icons/mmm.png")));

        
        logoLabel.setIcon(ProjectIcon.PISTACHIO_LOGO_BIG.icon());
        
        for (int i = 0; i < getComponents().length; i++)
            if(getComponent(i) instanceof JLabel){
                JLabel help = (JLabel) getComponent(i);
                help.setForeground(ProjectColors.TEXT_COLOR_PRIMARY.color());
            }
		
	}
	private void initGUI() {
		
		setSize(new Dimension(1731, 963));
		this.logoLabel = new JLabel();
		this.emailLabel = new JLabel();
		this.emailLabel.setVerticalTextPosition(SwingConstants.TOP);
		this.emailLabel.setText("      ");
		this.emailLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
		this.emailLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.phoneLabel = new JLabel();
		this.addLabel = new JLabel();
		this.siteAddLabel = new JLabel();
		this.titleLabel = new JLabel();
		this.titleLabel.setText("\u062F\u0631\u0628\u0627\u0631\u0647 \u06CC \u0645\u0627");
		this.titleLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		this.titleLabel.setFont(ProjectFont.LARGE_TEXT.font());
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(560, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(this.logoLabel, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(this.emailLabel, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
								.addComponent(this.phoneLabel, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(this.addLabel, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
									.addComponent(this.siteAddLabel, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)))
							.addGap(296))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(this.titleLabel, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(this.titleLabel, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
					.addGap(251)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(this.logoLabel, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(this.emailLabel, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(this.phoneLabel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(this.siteAddLabel, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(this.addLabel, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)))
					.addGap(355))
		);
		setLayout(groupLayout);
	}
	
	private void changePanel(JPanel panel) {
        IRPanel.removeAll();
        IRPanel.repaint();
        IRPanel.revalidate();

        IRPanel.add(panel);
        IRPanel.repaint();
        IRPanel.revalidate();
    }
	
	private void changeBackButtonEnteredIcon(JButton button) {

        button.setIcon(ProjectIcon.BACK_GREEN.icon());
    }
	
	private void changeBackButtonExitIcon(JButton button) {
        button.setIcon(ProjectIcon.BACK_WHITE.icon());
    }
	
	
	

}

