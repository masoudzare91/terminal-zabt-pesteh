package terminal;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.metal.MetalToggleButtonUI;

import DataModel.Admin;
import terminal.menuFramePanels.*;
import terminal.menuFramePanels.CustomerInfoPanels.CustomerInfoPanel;
import terminal.menuFramePanels.CustomerInfoPanels.CustomerInfoRootPanel;
import terminal.menuFramePanels.factorPanels.FactorRootPanel;
import terminal.menuFramePanels.reservePanels.ReserveRootPanel;
import terminal.menuFramePanels.storePanels.StorePanel;
import terminal.menuFramePanels.storePanels.StoreRootPanel;
import utils.*;
import org.junit.*;


@SuppressWarnings("serial")
public class MenuFrame extends JFrame implements MouseListener {

	private JPanel contentPane;
	private JPanel toolBarPanel;
	private Decorated decorated;
	private JPanel adminPanel;
	private JLabel infAdminLabel;
	private TemplateButton optionsBtn;
	private JPanel menuPanel;
	private JLabel menuLabel;
	private JPanel opPanel;
	private JPanel menuTitlePanel;
	private newCustomerPanel newCustomerPanel;
	private TemplateButton tempBtn;
	private CustomerInfoPanel customerInfoPanel;
	private TemplateButton newCustomerButton;
	private TemplateButton infCustomerButton;
	private TemplateButton reserveButton;
	private TemplateButton storeButton;
	private TemplateButton factorButton;
	private TemplateButton settingButton;
	private TemplateButton aboutButton;
	private FactorRootPanel factorRootPanel;
	private StorePanel storePanel;
	private StoreRootPanel storeRootPanel;
	
	private CustomerInfoRootPanel customerInfoRootPanel;
	private ReserveRootPanel reserveRootPanel;
	private AboutUspanel aboutUspanel;
	
	private JFrame loginFrame;
	
	private AdminPopUpMenu adminPopUpMenu;
	private JLabel lblNewLabel;



	private static Admin admin = new Admin();

	public static Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
        infAdminLabel.setText(admin.getName()+" "+admin.getLastName());
		this.admin = admin;

	}

//	public void testsetAdmin(){
//	text x = "admin";
//		setAdmin(data.model);
//	text testx = "admin";
//	assertArrayEq(testx,x);
//	}



	public MenuFrame(JFrame loFrame) {
		
		this.loginFrame = loFrame;
		
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(ProjectIcon.PISTACHIO_LOGO_SMALL.icon().getImage());
		setBounds(100, 100, 1106, 812);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.setContentPane(this.contentPane);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.contentPane.setPreferredSize(new Dimension(getHeight(),getWidth()));
		{
			this.toolBarPanel = new JPanel();
			this.toolBarPanel.setAlignmentY(1.0f);
			this.toolBarPanel.setAlignmentX(0.0f);
			this.contentPane.add(this.toolBarPanel, BorderLayout.NORTH);
			this.toolBarPanel.setLayout(new BorderLayout(0, 8));
			{
				this.decorated = new Decorated();
				this.decorated.setAlignmentY(2.0f);
				this.toolBarPanel.add(this.decorated, BorderLayout.NORTH);
			
				
				this.lblNewLabel = new JLabel("");
				this.decorated.add(this.lblNewLabel, BorderLayout.WEST);
			}
			{   
				this.adminPanel = new JPanel();
				this.adminPanel.setBorder(new MatteBorder(0, 0, 1, 0, (Color) ProjectColors.BLUE_GREY.color()));
				this.adminPanel.setBackground(ProjectColors.BACKGROUND.color());
				this.toolBarPanel.add(this.adminPanel);
				{
					
					this.infAdminLabel = new JLabel("\u062C\u0647\u0627\u0646\u06AF\u06CC\u0631 \u0634\u06A9\u0631\u0627\u0644\u0644\u0647\u06CC");
					this.infAdminLabel.setIcon(null);
					this.infAdminLabel.setFont(new Font("IRANSansWeb(FaNum) Light", Font.PLAIN, 17));
				}
				this.optionsBtn = new TemplateButton();
				this.optionsBtn.setBorderPainted(false);
				this.optionsBtn.setFocusPainted(false);
				this.optionsBtn.setBorder(null);
				this.optionsBtn.setIcon(new ImageIcon(MenuFrame.class.getResource("/icons/icons8_Sort_Down_20px.png")));
				this.menuTitlePanel = new JPanel();
				GroupLayout gl_adminPanel = new GroupLayout(this.adminPanel);
				gl_adminPanel.setHorizontalGroup(
					gl_adminPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adminPanel.createSequentialGroup()
							.addComponent(this.optionsBtn, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(this.infAdminLabel)
							.addPreferredGap(ComponentPlacement.RELATED, 736, Short.MAX_VALUE)
							.addComponent(this.menuTitlePanel, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE))
				);
				gl_adminPanel.setVerticalGroup(
					gl_adminPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adminPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_adminPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(this.optionsBtn, 0, 0, Short.MAX_VALUE)
								.addComponent(this.infAdminLabel, GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)))
						.addComponent(this.menuTitlePanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
				);
				this.menuTitlePanel.setLayout(new BorderLayout(0, 0));
				this.menuLabel = new JLabel("\u0645\u0646\u0648\u06CC \u06A9\u0627\u0631\u0628\u0631\u06CC");
				this.menuLabel.setAlignmentX(0.5f);
				this.menuTitlePanel.add(this.menuLabel);
				this.menuLabel.setHorizontalTextPosition(SwingConstants.LEFT);
				this.menuLabel.setHorizontalAlignment(SwingConstants.RIGHT);
				this.menuLabel.setIcon(null);
				this.menuLabel.setFont(new Font("IRANSansWeb(FaNum) Light", Font.PLAIN, 17));
				optionsBtn.setBackground(ProjectColors.BACKGROUND.color());
				//optionsBtn.setModel(new FixedStateButtonModel());
				this.adminPanel.setLayout(gl_adminPanel);
			}
		}
		{
			this.menuPanel = new JPanel();
			this.contentPane.add(this.menuPanel, BorderLayout.EAST);
			this.newCustomerButton = new TemplateButton((ImageIcon) null, null);
			this.newCustomerButton.setIconTextGap(10);
			this.newCustomerButton.setText("\u0645\u0634\u062A\u0631\u06CC \u062C\u062F\u06CC\u062F     ");
			this.infCustomerButton = new TemplateButton((ImageIcon) null, null);
			this.infCustomerButton.setText("\u0627\u0637\u0644\u0627\u0639\u0627\u062A \u0645\u0634\u062A\u0631\u06CC  ");
			this.reserveButton = new TemplateButton((ImageIcon) null, null);
			this.reserveButton.setText("\u0646\u0648\u0628\u062A \u062F\u0647\u06CC          ");
			this.storeButton = new TemplateButton((ImageIcon) null, null);
			this.storeButton.setText("  \u0627\u0646\u0628\u0627\u0631                ");
			this.factorButton = new TemplateButton((ImageIcon) null, null);
			this.factorButton.setText("\u0635\u062F\u0648\u0631 \u0641\u0627\u06A9\u062A\u0648\u0631       ");
			this.settingButton = new TemplateButton((ImageIcon) null, null);
			this.settingButton.setText("\u062A\u0646\u0638\u06CC\u0645\u0627\u062A            ");
			this.aboutButton = new TemplateButton((ImageIcon) null, null);
			this.aboutButton.setText("\u062F\u0631\u0628\u0627\u0631\u0647 \u06CC \u0645\u0627          ");
			GroupLayout gl_menuPanel = new GroupLayout(this.menuPanel);
			gl_menuPanel.setHorizontalGroup(
				gl_menuPanel.createParallelGroup(Alignment.TRAILING)
					.addComponent(this.infCustomerButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.newCustomerButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.reserveButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.storeButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.factorButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.settingButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addComponent(this.aboutButton, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
			);
			gl_menuPanel.setVerticalGroup(
				gl_menuPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_menuPanel.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.newCustomerButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.infCustomerButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.reserveButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.storeButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.factorButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.settingButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 338, Short.MAX_VALUE)
						.addComponent(this.aboutButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
			);
			this.menuPanel.setLayout(gl_menuPanel);
		}
		{
			this.opPanel = new JPanel();
			this.contentPane.add(this.opPanel, BorderLayout.CENTER);
			this.opPanel.setLayout(new CardLayout(0, 0));
		}
		
		myCustomGui();
	}
	
	private void myCustomGui() {
		tempBtn = new TemplateButton(null , null);
		customerInfoPanel = new CustomerInfoPanel();
		newCustomerPanel = new newCustomerPanel();
		factorRootPanel = new FactorRootPanel();
		storePanel = new StorePanel();
		adminPopUpMenu = new AdminPopUpMenu("adminMenu", loginFrame, this);
		customerInfoRootPanel = new CustomerInfoRootPanel();
		storeRootPanel = new StoreRootPanel();
		reserveRootPanel = new ReserveRootPanel();
		aboutUspanel = new AboutUspanel();
		
		infAdminLabel.setIcon(ProjectIcon.Admin_32px.icon());
		infAdminLabel.setHorizontalTextPosition(SwingConstants.LEFT);


		optionsBtn.setBorder(LineBorder.createBlackLineBorder());
		
		optionsBtn.setUI(new MetalToggleButtonUI() {
		    @Override
		    protected Color getSelectColor() {
		        return ProjectColors.BACKGROUND.color();
		    }
		});
		
		optionsBtn.addMouseListener( new MouseAdapter(){
			
			 @Override
		      public void mousePressed(MouseEvent e) {
		        final boolean shown = adminPopUpMenu.isPopupShown();
		        SwingUtilities.invokeLater(new Runnable() {
		 
		          @Override
		          public void run() {
		            adminPopUpMenu.setPopupShown(shown);
		          }
		        });
		      }
		});
		
		optionsBtn.addActionListener(e->{
			
			        if (adminPopUpMenu.isPopupShown()) {
			          optionsBtn.setIcon(ProjectIcon.SORT_DOWN.icon());
			          adminPopUpMenu.setVisible(false);
			          adminPopUpMenu.setPopupShown(false);
			        } else {
			        	
			          optionsBtn.setIcon(ProjectIcon.SORT_UP.icon());
			          adminPopUpMenu.show(optionsBtn, optionsBtn.getX() , optionsBtn.getY()+29);
			        }
			   
		});
		menuPanel.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
		
		menuBtnSetProperty();
		
		menuTitlePanel.setBackground(ProjectColors.ORANGE_COLOR_BTN.color());
		menuPanel.setBackground(ProjectColors.BLUE_GREY.color());
		menuLabel.setForeground(ProjectColors.WHITE_BTN.color());
		menuLabel.setIcon(ProjectIcon.MENU_WHITE.icon());


		}
	
	private void menuBtnSetProperty() {
		setButtonFont();
		setIcons(newCustomerButton , ProjectIcon.ADD_USER_WHITE.icon() ,12 );
		setIcons(infCustomerButton , ProjectIcon.USER_INF_WHITE.icon() ,12 );
		setIcons(storeButton , ProjectIcon.PM_ICON.icon() ,20 );
		setIcons(factorButton , ProjectIcon.PURCHASE_WHITE.icon() ,12 );
		setIcons(reserveButton, ProjectIcon.EVENT_WHITE.icon(), 12);
		setIcons(settingButton, ProjectIcon.SETTING_WHITE.icon(), 12);
		setIcons(aboutButton, ProjectIcon.INFO_32PX_WHITE.icon(), 12);

		addMouseListenerToMenuBtn();
		setMenuBtnBackGround();
		setMenuBtnBorder();
	}



	private void setButtonFont() {
		
		Component[] btns = menuPanel.getComponents();
		TemplateButton button = new TemplateButton();
		for (int i = 0; i < btns.length; i++) {
			
			button = (TemplateButton) btns[i];
			button.setFont(ProjectFont.BUTTONS.font());
			button.setForeground(ProjectColors.WHITE_BTN.color());
		}
	}

	private void setIcons(TemplateButton btn , ImageIcon icon , int gap) {
		btn.setUnSelectIcon(icon);
		btn.setIcon(btn.getUnSelectIcon());
		btn.setHorizontalTextPosition(SwingConstants.LEADING);
		btn.setIconTextGap(gap);
		/*infCustomerButton;
		pistachioButton;
		reserveButton;
		storeButton;
		factorButton;
		settingButton;
		aboutButton;*/
		
	}

	private void addMouseListenerToMenuBtn() {
		
		Component[] btns = menuPanel.getComponents();
		TemplateButton button = new TemplateButton(null , null);
		for (int i = 0; i < btns.length; i++) {
			button = (TemplateButton) btns[i];
			button.addMouseListener(this);
		}
		
	}

	private void setMenuBtnBorder() {
		
		Component[] btns = menuPanel.getComponents();
		TemplateButton button = new TemplateButton(null , null);
		for (int i = 0; i < btns.length; i++) {
			button = (TemplateButton) btns[i];
			button.setBorder(new MatteBorder(0, 0, 0, 7, (Color) ProjectColors.BLUE_GREY.color()));
		}
		
		
	}
	
	                    
	private void setMenuBtnBackGround() {
		
		Component[] btns = menuPanel.getComponents();
		TemplateButton button = new TemplateButton(null, null);
		for (int i = 0; i < btns.length; i++) {
			button = (TemplateButton) btns[i];
			button.setBackground(ProjectColors.BLUE_GREY.color());	
		}
		
	}

private void changePanel(JPanel panel) {

		try {
				opPanel.removeAll();
		        opPanel.repaint();
		        opPanel.revalidate();

		        opPanel.add(panel);
		        
		        opPanel.repaint();
		        opPanel.revalidate();
		       
		} catch (Exception e) {
			
		}
        
    }

	private void ButtonsClickEvent(TemplateButton btn, JPanel newPanel) {
	
		tempBtn.setBorder(new MatteBorder(0, 0, 0, 7, (Color) ProjectColors.BLUE_GREY.color()));
		tempBtn.setSelected(false);
	
		tempBtn = btn;
		
	
		tempBtn.setSelected(true);
		tempBtn.setBorder(new MatteBorder(0, 0, 0, 7, (Color) ProjectColors.ORANGE_COLOR_BTN.color()));
		tempBtn.setBackground(ProjectColors.BLUE_GREY.color());
	
		changePanel(newPanel);
	
	}
	
	private void BtnHoverEnter(Object source) {
		
		TemplateButton button = (TemplateButton) source;
		
		if(!button.isSelected()) { 
			button.setBackground(ProjectColors.ORANGE_COLOR_BTN.color());
			button.setBorder(new MatteBorder(0, 0, 0, 7, (Color) ProjectColors.ORANGE_COLOR_BTN.color()));
		}
		
		
	}
	
	private void BtnHoverExited(Object source) {
		TemplateButton button = (TemplateButton) source;
		
		if(!button.isSelected()) {
			button.setBackground(ProjectColors.BLUE_GREY.color());
			button.setBorder(new MatteBorder(0, 0, 0, 7, (Color) ProjectColors.BLUE_GREY.color()));
		}
		
	}
	
	
	//override methods
	@Override
	public void mouseClicked(MouseEvent e) {
		
		if(e.getSource() == newCustomerButton) 
			ButtonsClickEvent(newCustomerButton , newCustomerPanel);
		else if(e.getSource() == infCustomerButton)
			ButtonsClickEvent(infCustomerButton , customerInfoRootPanel);
		else if(e.getSource() == storeButton)
			ButtonsClickEvent(storeButton , storeRootPanel);
		else if(e.getSource() == aboutButton)
			ButtonsClickEvent(aboutButton, aboutUspanel);
		else if(e.getSource() == settingButton)
			ButtonsClickEvent(settingButton, null);
		else if(e.getSource() == factorButton)
			ButtonsClickEvent(factorButton, factorRootPanel);
		else if(e.getSource() == reserveButton)
			ButtonsClickEvent(reserveButton, reserveRootPanel);

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
		if(e.getSource() instanceof TemplateButton)
			BtnHoverEnter(e.getSource());
			
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(e.getSource() instanceof TemplateButton)
			BtnHoverExited(e.getSource());
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		
	}
}
